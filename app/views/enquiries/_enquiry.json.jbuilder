json.extract! enquiry, :id, :first_name, :last_name, :zipcode, :course, :email, :created_at, :updated_at
json.url enquiry_url(enquiry, format: :json)