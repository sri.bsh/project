class CreateEnquiries < ActiveRecord::Migration[5.0]
  def change
    create_table :enquiries do |t|
      t.string :first_name
      t.string :last_name
      t.integer :zipcode
      t.string :course
      t.string :email

      t.timestamps
    end
  end
end
